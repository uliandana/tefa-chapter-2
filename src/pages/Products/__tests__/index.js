import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import ShallowRenderer from 'react-test-renderer/shallow';
import data from '../product.json';
import Products from '../Products';

jest.mock('react-router-dom', () => ({
  Link: () => {
    const link = props => <link {...props} />;
    return link;
  },
  useLocation: jest.fn(() => ({
    state: null,
  })),
}));

// jest mock komponen yang diimport tidak wajib di tugas ini
// Tapi sebaiknya perlu. Kenapa?
// Karena supaya kalau ke depannya komponen Table atau InputField ada perubahan,
// unit test kita di file ini dan file lainnya yang import komponen tersebut tidak terganggu
jest.mock('../../../components/elements/Table/Table', () => {
  const table = props => <table {...props} />;
  return table;
});
jest.mock('../../../components/elements/Modal', () => {
  const modal = props => <modal {...props} />;
  return modal;
});
jest.mock('../../../components/elements/Button', () => {
  const button = props => <button {...props} />;
  return button;
});
jest.mock('../../../components/elements/Select/Select', () => {
  const select = props => <select {...props} />;
  return select;
});
jest.mock('../../../components/elements/InputField/InputFIeld', () => {
  const inputField = props => <input {...props} />;
  return inputField;
});

describe('src/pages/Products', () => {
  test('test render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Products />);
    expect(tree).toMatchSnapshot();
  });

  test('setItemsData when useLocation state not null', () => {
    // mock local supaya state dari useLocation isinya dummy item
    const newItem = {
      'id': 'sku1001',
      'name': 'New Item',
      'price': 2000,
      'image': 'http://dummyimage.com/200x200.png/ff4444/ffffff',
      'description': 'desc',
      'isDeleted': false,
      'category': 'Pre-Order',
      'expiryDate': '2022-07-24',
    };
    useLocation.mockImplementationOnce(() => ({
      state: newItem,
    }));

    // function jest.fn bisa dipakai untuk expect()
    // supaya bisa masuk kedalam kode, jest.fn disisipin ke dalam useState
    // sebutan lainnya spyOn https://jestjs.io/docs/jest-object#jestspyonobject-methodname
    const setItemsData = jest.fn();
    useState.mockImplementationOnce(v => [v, setItemsData]);


    // eksekusi (invoke)
    Products();

    // expect
    expect(setItemsData).toHaveBeenCalledWith([newItem, ...data]);
  });

  test('render rowData with & without opacity', () => {
    // eksekusi (invoke)
    const res = Products();
    const table = res.props.children[2];

    // expect
    const withOpacity = table.props.rows(data[0], 0);
    expect(withOpacity.props.children.props.className).toBe('p-2 opacity-40');

    // satu test case bisa lebih dari satu expect untuk menghemat proses invoke
    const withoutOpacity = table.props.rows(data[1], 1);
    expect(withoutOpacity.props.children.props.className.trim()).toBe('p-2');
  });

  test('onDelete', () => {
    // sisipkan setItemsData karena nanti akan diexpect
    const setItemsData = jest.fn();
    useState.mockImplementationOnce(v => [v, setItemsData]);

    // eksekusi (invoke)
    const res = Products();
    const table = res.props.children[2];
    const row = table.props.rows(data[0], 0);
    const modal = row.props.children.props.children[4].props.children;
    
    // invoke onDelete
    modal.props.onDelete();
    // expect, karena bentuknya array of object jadi perlu dibongkar ke dalam pakai arrayContaining dan objectContaining
    expect(setItemsData).toHaveBeenCalledWith(
      expect.arrayContaining([
        expect.objectContaining({ id: data[0].id, isDeleted: true })
      ])
    );
  });
});
